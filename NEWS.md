## 0.1.2 (2019-06-30)

- Implement data downloads.

### Bug fixes

- When initializing the app, `data_input()` caused an error in  creating a new
  `WFSClient` as inputs were not yet available. Using `req()` for fixes this.

### New features

- It is now possible to download the spatial data in three different formats:
  
  1. Shapefile
  2. GeoPackage
  3. Vector graphics
  
  The first two are GIS compatible spatial formats and the last one is
  non-spatial format suitable for generic graphics.

### UI changes

- Download button + format radio buttons added.
